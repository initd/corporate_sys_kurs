/*
* @Author:
* @Date:   2014-09-27 15:17:10
* @Last Modified by:   user
* @Last Modified time: 2014-09-27 20:00:01
*/

function strToIntSorter(a, b) {
        a = +a;
        b = +b;
        if (a > b) return 1;
        if (a < b) return -1;
        return 0;
}

function handleRedirectResponse(data) {
    document.open();
    document.write(data);
    document.close();
}

$(document).ready(function(){

    $("#valid_thru").datetimepicker({
        language: "ru",
        useSeconds: false,
        format: "YYYY-MM-DD HH:mm"
    });

    $("select[multiple]").multiselect({
        numberDisplayed: 10,
        includeSelectAllOption: true,
        nonSelectedText: "Не отправлять"
    });


    $("#admin_users_table").bootstrapTable();
    $("#admin_stores_table").bootstrapTable();
    $("#admin_hosts_table").bootstrapTable();

    /* Handle "Find user" buttons */
    $(".user_search").click(function(event){
        var value = $(".user-ident-search-form").val();
        if (!value) {
            event.preventDefault();
            alertify.alert("Должен быть указан email или ID пользователя.");
        } else {
            $('<form id="form_search_user" method="GET" action="/admin/users"></form>').appendTo('body');
            form = $("#form_search_user");
            form.append('<input type="hidden" name="ident" value="' + value + '" />');
            form.submit();
        }       

    });

    $(".store-update").click(function(event){
        var obj = $(this);
        var action = obj.attr("action");
        var url = obj.attr("url");
        if (!action || !url) {
            event.preventDefault();
            alertify.alert("Не указано действие над записью или URI объекта.");
        } else {
            alertify.confirm("Вы действительно хотите обновить статус записи?",
                function(e) {
                if (e) {
                    $('<form id="form_change_store" method="POST" action="' + url + '/change"></form>').appendTo('body');
                    form = $("#form_change_store");
                    form.append('<input type="hidden" name="action" value="' + action + '" />');
                    form.submit();
                }
            });
        }
    });

    $(".store-delete").click(function(){
        var obj = $(this);
        var url = obj.attr("url");
        alertify.confirm("Вы действительно хотите удалить запись?", function(e){
            if(e) {
                $('<form id="form_delete_store" method="POST" action="' + url + '/delete"></form>').appendTo('body');
                form = $("#form_delete_store");
                form.submit();
            }
        });
    });

    $(".host-delete").click(function(){
        var obj = $(this);
        var url = obj.attr("url");
        alertify.confirm("Вы действительно хотите удалить запись?", function(e){
            if(e) {
                $('<form id="form_unban_host" method="POST" action="' + url + '/unban"></form>').appendTo('body');
                form = $("#form_unban_host");
                form.submit();
            }
        });
    });

});