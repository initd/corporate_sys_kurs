# coding: utf-8
import datetime as dt

from py_complex_web.application.workers import messages_sender

datetime_fmt = "%Y-%m-%d %H:%M:%S"


def main():
    try:
        messages_sender.main()
    except Exception as e:
        print dt.datetime.now().strftime(datetime_fmt), e


if __name__ == '__main__':
    main()
