# coding: utf-8


class PaginatorError(Exception):
    def __init__(self, code=None, message=None):
        self.code = code
        self.message = message


class Paginator(object):
    def __init__(self, **kwargs):
        self.template = kwargs.get("template")
        self.curr_page = kwargs.get("curr_page", 1)
        self.items_per_page = kwargs.get("items_per_page", 25)
        self.total_items_count = kwargs.get("total_items_count", 0)
        self.pages_before_curr = kwargs.get("pages_before_curr", 5)
        self.pages_after_curr = kwargs.get("pages_before_curr", 5)

    @property
    def has_next(self):
        return self.total_pages > self.curr_page

    @property
    def has_prev(self):
        return self.curr_page > 1

    @property
    def total_pages(self):
        page = self.total_items_count * 1. / self.items_per_page
        if page != int(page):
            page += 1
        return int(page)

    @property
    def a_next(self):
        if self.template is None:
            raise PaginatorError(message="Template is not set")
        return self.template.format(page=self.curr_page + 1)

    @property
    def a_first(self):
        if self.template is None:
            raise PaginatorError(message="Template is not set")
        return self.template.format(page=1)

    @property
    def a_last(self):
        if self.template is None:
            raise PaginatorError(message="Template is not set")
        return self.template.format(page=self.total_pages)

    @property
    def a_prev(self):
        if self.template is None:
            raise PaginatorError(message="Template is not set")
        return self.template.format(page=self.curr_page - 1)

    @property
    def prev_links(self):
        page = self.curr_page - self.pages_before_curr
        if page < 1:
            page = 1
        while page < self.curr_page:
            yield self.template.format(page=page), page
            page += 1

    @property
    def next_links(self):
        page = self.curr_page
        while True:
            if page - self.curr_page >= self.pages_after_curr:
                break
            if page >= self.total_pages:
                break
            page += 1
            yield self.template.format(page=page), page
