# coding: utf-8
from hashlib import md5

from bottle import abort
from bottle import request
from bottle import redirect

from py_complex_web import config


def require_auth(func):
    def call(*args, **kwargs):
        if request.session.is_guest:
            return redirect("/users/login?redirect_url=%s" % request.path)
        return func(*args, **kwargs)
    return call


def require_admin(func):
    def call(*args, **kwargs):
        if not request.session.data.get("is_admin"):
            return redirect("/users/login?redirect_url=%s" % request.path)
        return func(*args, **kwargs)
    return call


def require_session_email(func):
    def call(*args, **kwargs):
        if not request.session.data.get("email"):
            return abort(403, text=u"Доступ запрещен, попробуйте заново")
        return func(*args, **kwargs)
    return call


def get_salt():
    return config.TOKEN_SALT


def get_host():
    return config.HOST


class TokenGenerator(object):
    def __init__(self):
        self.salt = get_salt()
        self.host = get_host()

    def generate_email_activation_token(self, email):
        _str = "%s:%s" % (self.salt, email)
        return md5(_str).hexdigest()

    def generate_email_activation_link(self, email):
        token = self.generate_email_activation_token(email)
        link = "http://%s/users/activate?email=%s&token=%s"
        return link % (self.host, email, token)

    def generate_password_change_token(self, curr_psw):
        _str = "%s:%s" % (self.salt, curr_psw)
        return md5(_str).hexdigest()

    def generate_password_change_link(self, email, curr_psw):
        token = self.generate_password_change_token(curr_psw)
        link = "http://%s/users/password/set?email=%s&token=%s"
        return link % (get_host(), email, token)
