# coding: utf-8
""" Class for working with user session.

Note! It's not only session data storage, it's a wrappers around
user role operations, permissions and access control.

NB! Default session time is 15 minutes

TODO: session keys reverse mapping as login: session_key

"""

import redis
import cPickle

from time import time
from hashlib import md5
from abc import abstractmethod


class AbstractSessionStorage(object):
    def __init__(self, key, settings=None):
        self.key = key
        self.settings = {"prefix": "sess_"}
        if type(settings) is dict:
            self.settings.update(settings)

    @abstractmethod
    def read(self):
        raise NotImplementedError

    @abstractmethod
    def write(self, data, expire=None):
        raise NotImplementedError

    @abstractmethod
    def delete(self):
        raise NotImplementedError

    def key_build(self):
        return "%s%s" % (self.settings["prefix"], self.key)

    def key_parse(self):
        return self.key.replace(self.settings["prefix"], "")

    @staticmethod
    def data_pack(data):
        return cPickle.dumps(data)

    @staticmethod
    def data_unpack(string):
        return cPickle.loads(string)


class FilesSessionStorage(AbstractSessionStorage):
    def __init__(self, key, settings):
        super(FilesSessionStorage, self).__init__(key, settings)
        self.file_name = settings.get("filename", "sessions.file")
        self.sessions_dir = settings.get("sessions_dir", '../sessions/')

    def _get_session_file_name(self):
        return self.sessions_dir + self.key_build() + '_' + self.file_name

    def read(self):
        try:
            with open(self._get_session_file_name(), 'rb') as session_file:
                data = session_file.read()
                if data:
                    data = self.data_unpack(data)
        except IOError:
            data = None
        return data

    def write(self, data, expire=None):
        data = self.data_pack(data)
        with open(self._get_session_file_name(), 'wb+') as session_file:
            session_file.write(data)

    def delete(self):
        import os
        try:
            os.remove(self._get_session_file_name())
        except OSError:
            pass


class RedisSessionStorage(AbstractSessionStorage):
    def __init__(self, key, settings):
        super(RedisSessionStorage, self).__init__(key, settings)

        self.redis = redis.StrictRedis(
            db=settings.get("db", 0),
            port=settings.get("port", 6379),
            host=settings.get("host", "127.0.0.1")
        )

    def write(self, data, expire=None):
        if expire is None:
            expire = 60 * 15
        key = self.key_build()
        value = self.data_pack(data)
        self.redis.setex(key, int(expire), value)

    def read(self):
        key = self.key_build()
        string = self.redis.get(key)
        if string:
            return self.data_unpack(string)
        return None

    def delete(self):
        key = self.key_build()
        self.redis.delete(key)


class Session(object):
    data = None
    request = None
    response = None
    auto_save = False
    auto_start = False
    session_key = None
    session_salt = None
    session_time = None

    @property
    def id_session(self):
        return self.data["id_session"]

    @id_session.setter
    def id_session(self, value):
        self.data["id_session"] = value
        self.storage = self._reinit_storage()

    @property
    def is_user(self):
        return "id_user" in self

    @property
    def is_guest(self):
        return not self.is_user

    def __init__(self, request=None, response=None, **kwargs):
        """ Objects copy """
        self.request = request
        self.response = response

        self.auto_save = kwargs.pop("auto_save", False)
        self.auto_start = kwargs.pop("auto_start", False)
        self.session_key = kwargs.pop("session_key", "id_session")
        self.session_salt = kwargs.pop("session_salt", "UII@*A*@&@")
        self.session_time = kwargs.pop("session_time", 60*15)

        """ Storage settings """
        self.storage = None
        self.storage_type = kwargs.pop("storage_type", "files")
        self.storage_settings = kwargs.pop("storage_settings", {})

        self.clear()

        if self.auto_start:
            self.start()

    def __getitem__(self, name):
        if name in self.__dict__:
            return self.__dict__[name]
        if name in self.data:
            return self.data[name]
        raise AttributeError(name)

    def __setitem__(self, name, value):
        if name in self.__dict__:
            self.__dict__[name] = value
        else:
            self.data[name] = value

    def __delitem__(self, name):
        del self.data[name]

    def __repr__(self):
        return "%s" % self.data

    def __contains__(self, key):
        if key in self.__dict__:
            return True
        return key in self.keys()

    def keys(self):
        return self.data.keys()

    def values(self):
        return self.data.values()

    def items(self):
        return self.data.items()

    def clear(self):
        try:
            self.storage.delete()
        except AttributeError:
            pass

        self.data = {
            "id_session": None,
            "session_time_updated": time(),
            "session_time_created": time(),
        }

    def start(self):
        self.id_session = self.request.get_cookie(self.session_key)

        if self.id_session:
            self._read_data()
        else:
            self._start_new()
            self.process_autosave()

    def invalidate(self):
        self._start_new()

    def process_autosave(self):
        if self.auto_save:
            self.save()

    def save(self):
        self.data["session_time_updated"] = time()
        self._get_storage().write(self.data, expire=self.session_time)
        self.response.set_cookie(self.session_key, self.id_session,
                                 path="/", max_age=self.session_time)

    def _start_new(self):
        self.clear()
        self.id_session = self._generate_id()

    def _read_data(self):
        data = self._get_storage().read() or {}
        self.data.update(data)

    def _get_storage(self):
        if self.storage:
            return self.storage

        return self._reinit_storage()

    def _reinit_storage(self):
        if self.storage_type == "redis":
            self.storage = RedisSessionStorage(self.id_session,
                                               self.storage_settings)
        elif self.storage_type == "files":
            self.storage = FilesSessionStorage(self.id_session,
                                               self.storage_settings)
        else:
            raise NotImplementedError("Storage type %s not implemented" %
                                      self.storage_type)
        return self.storage

    def _generate_id(self):
        _str = "%s:%s" % (self.session_salt, time())
        return md5(_str).hexdigest()


class SessionLoginDictMixIn(object):
    """
    Mixin for store reverse dict login: id_session
    """
    def __init__(self):
        self.storage = None

    def store_login_session_rel(self, login):
        if self.storage.settings["is_user_unique"]:
            # search for user session in reverse dict
            # delete from storage and reverse all sessions except current
            pass
        else:
            self.storage


if __name__ == "__main__":
    session_settings = {
        "session_key": "PHPSESSID",
        "session_salt": "USJ@*!KLJA*@!@@!MLUD*@",
        "session_time": 300,

        "auto_start": True,

        "storage_type": "redis",
        "storage_settings": {
            "db": 0,
            "host": "127.0.0.1",
            "port": 6379,
            "prefix": "sess_prefix",
            "reverse_pref": "reverse_pref_",
            "is_user_unique": False,
        }
    }

    sess = Session(settings=session_settings)
    sess["user_name"] = "Aaa"
    sess.save()
