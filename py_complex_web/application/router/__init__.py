# coding: utf-8
from py_complex_web.application.router.admin.routes import *
from py_complex_web.application.router.dashboard.routes import *
from py_complex_web.application.router.users.routes import *
