# coding: utf-8
from bottle import jinja2_view as view
from bottle import route

from py_complex_web.db.models import User
from py_complex_web.lib.response import response_ok
from py_complex_web.lib.auth import require_auth, require_admin


@route("/admin")
@require_auth
@require_admin
@view("views/admin/index.html")
def route_admin_dashboard():
    total_users = User.get_items_count()
    data = dict(total_users=total_users)
    return response_ok({"data": data})
