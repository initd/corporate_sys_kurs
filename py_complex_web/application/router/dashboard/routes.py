# coding: utf-8
import datetime as dt

from bottle import route
from bottle import jinja2_view as view

from py_complex_web.db.models import User
from py_complex_web.lib.response import response_ok
from py_complex_web.lib.time_helper import dt_to_unixtime


def days_to(days):
        return dt_to_unixtime(dt.datetime.now() + dt.timedelta(days=days))


@route("/")
@view("views/index.html")
def route_index():
    total_users = User.get_items_count()
    data = dict(total_users=total_users)
    return response_ok({"data": data})
