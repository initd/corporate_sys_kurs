# coding: utf-8
from py_complex_web.application.notificator import messages_senders
from py_complex_web.application.hotqueue.conn import messages_queue


def main():
    email = messages_senders.Email()
    for message in messages_queue.consume():
        del message["type"]
        email.send_email(**message)


if __name__ == '__main__':
    main()
