# coding: utf-8
# from pony.orm import Set
from pony.orm import Optional
from pony.orm import Required
from pony.orm import PrimaryKey

from py_complex_web.db.conn import db
from py_complex_web.db.mixins import SiteItem
from py_complex_web.lib.time_helper import unixtime_to_date
# from py_complex_web.lib.time_helper import unixtime_to_datetime_tz


default_date_fmt = "%d-%m-%Y"
default_datetime_fmt = "%d-%m-%Y %H:%M:%S"


class User(SiteItem, db.Entity):
    _table_ = "users"
    id = PrimaryKey(int, auto=True)
    email = Required(unicode, 255, unique=True)
    password = Required(unicode, 64)
    time_created = Required(int)
    time_activated = Optional(int, default=0)
    time_locked = Optional(int, default=0)
    time_deleted = Optional(int, default=0)

    @property
    def datecreated(self):
        return unixtime_to_date(self.time_created, default_date_fmt)

db.generate_mapping(create_tables=True)
